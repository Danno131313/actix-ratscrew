#!/bin/bash

set -e

mkdir -p ~/.ssh
echo -e "$SSH_KEY" > ~/.ssh/id_ed25519
chmod 600 ~/.ssh/id_ed25519

mkdir -p ~/.ssh
touch ~/.ssh/config
echo -e "Host *\n\tStrictHostKeyChecking no \n\n" >> ~/.ssh/config

echo "Copying artifacts to server..."
scp ./target/release/actix-ratscrew root@$SERVER_IP:bin/actix-ratscrew_new
ssh -q root@$SERVER_IP rm -rf /var/www/front_end/*
scp -qr ./ratscrew-client/dist/* root@$SERVER_IP:bin/front_end/

echo "Running updateAndDeploy script on server..."
ssh -q root@$SERVER_IP "bash" < deploy/updateAndRestart.sh
