#!/bin/bash

set -e

cd bin

echo "Stopping Ratscrew service..."
systemctl stop ratscrew.service

echo "Stopping nginx service..."
systemctl stop nginx.service

echo "Copying new files..."
rm actix-ratscrew
mv actix-ratscrew_new actix-ratscrew
mv front_end/* /var/www/front_end/

echo "Restarting Ratscrew service..."
systemctl start ratscrew.service

echo "Restarting nginx service..."
systemctl start nginx.service
service nginx restart
