# actix-ratscrew

[![pipeline status](https://gitlab.com/Danno131313/actix-ratscrew/badges/master/pipeline.svg)](https://gitlab.com/Danno131313/actix-ratscrew/commits/master)

Runs on stable rust 1.30. Make sure you have an environment variable `VUE_APP_WS_URL` set for the websocket url (default is ws://localhost:8000/ws/ ). Run `yarn install` or `npm install` in the `ratscrew-client` directory, then run `yarn run serve` or `npm run serve`. In a different terminal, run `cargo run` from the main directory. The website should then be up and running at http://localhost:8080/ .
