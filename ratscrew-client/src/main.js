import Vue from 'vue'
import App from './App.vue'
import VueNativeSock from 'vue-native-websocket';
import dotenv from 'dotenv';

dotenv.config();

let wsurl = process.env.VUE_APP_WS_URL;

if (wsurl == "") {
    wsurl = "ws://localhost:8000/ws/";
}

Vue.use(VueNativeSock, wsurl, {
  connectManually: true,
  format: "json",
});

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  methods: {
    connect() {
      this.$connect();
    },
    disconnect() {
      this.$disconnect();
    },
    send(obj) {
      this.$socket.sendObj(obj);
    }
  }
}).$mount('#app')
