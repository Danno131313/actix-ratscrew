use actix::{self, Arbiter};
use actix_web::{
    http::header,
    middleware::{self, cors::Cors},
    server, App,
};
use env_logger;
use log::info;
use std::env;

mod game;
mod models;
mod routes;

use crate::{
    models::{game::GameServer, slap_helper::SlapHelper, ws::WsGameSessionState},
    routes::ws::{game_route, kick_player},
};

static PORT: u32 = 8000;

fn main() {
    env::set_var("RUST_LOG", "info,actix_web=info,actix_net=info");
    env_logger::init();

    let sys = actix::System::new("webapp");

    let game_server = Arbiter::start(|_| GameServer::new());
    let game_server_clone = game_server.clone();
    let slap_helper = Arbiter::start(move |_| SlapHelper::new(game_server_clone));

    let app = move || {
        App::with_state(WsGameSessionState {
            addr: game_server.clone(),
            slap_helper_addr: slap_helper.clone(),
        })
        .middleware(middleware::Logger::default())
        .configure(|app| {
            Cors::for_app(app)
                .allowed_methods(vec!["GET", "POST"])
                .allowed_headers(vec![header::AUTHORIZATION, header::ACCEPT])
                .allowed_header(header::CONTENT_TYPE)
                .max_age(3600)
                .resource("/ws/", |r| r.f(game_route))
                .register()
        })
        .resource("/kick/{id}", |r| r.with(kick_player))
    };

    server::new(app)
        .bind(format!("127.0.0.1:{}", PORT))
        .expect("Couldn't start server")
        .start();

    info!("Running on port {}", PORT);

    sys.run();
}
