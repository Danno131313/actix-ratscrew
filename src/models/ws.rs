use actix::{Addr, Message};
use actix_web::Error;
use simple_cards::Deck;
use uuid::Uuid;

use crate::models::{
    game::{GameServer, WsGameSession},
    slap_helper::SlapHelper,
};

// State holding the GameServer, passed between requests
pub struct WsGameSessionState {
    pub addr: Addr<GameServer>,
    pub slap_helper_addr: Addr<SlapHelper>,
}

pub struct Connect {
    pub addr: Addr<WsGameSession>,
}

pub struct Disconnect {
    pub id: Uuid,
    pub name: String,
    pub deck: Option<Deck>,
}

pub struct KickPlayer(pub Uuid);

pub struct GetKicked;

impl Message for Connect {
    type Result = Result<Uuid, Error>;
}

impl Message for Disconnect {
    type Result = ();
}

impl Message for KickPlayer {
    type Result = ();
}

impl Message for GetKicked {
    type Result = ();
}
