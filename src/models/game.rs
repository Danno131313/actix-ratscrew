use actix::{Addr, Message};
use serde_derive::{Deserialize, Serialize};
use serde_json::Value;
use simple_cards::{Card, Deck};
use std::{collections::HashMap, convert::From, time::Instant};
use uuid::Uuid;

use crate::models::slap_helper::SlapHelper;

// Server / Game state
pub struct GameServer {
    pub sessions: HashMap<Uuid, Addr<WsGameSession>>,
    pub pile: Deck,
    pub current_turn_idx: usize,
    pub turn_order: Vec<Uuid>,
    pub facecard_attempts_left: Option<usize>,
    pub faceoff_winner: Option<Uuid>,
    pub slap_helper_addr: Option<Addr<SlapHelper>>,
    pub slapped: Instant,
}

// State for each client
pub struct WsGameSession {
    pub id: Uuid,
    pub name: String,
    pub hand: Deck,
    pub hb: Instant,
    pub missed_slaps: usize,
    pub banned: bool,
}

// Messages to send to the GameServer
#[derive(Clone, Debug)]
pub enum GameServerMessage {
    Connected {
        name: String,
        id: Uuid,
    },
    Play {
        card: Card,
        id: Uuid,
        name: String,
        last_card: bool,
    },
    Slap {
        id: Uuid,
        name: String,
        time: u64,
    },
    NewGame,
    GameWon {
        name: String,
        cards_left: usize,
    },
}

// Messages to send to each client
#[derive(Clone, Debug)]
pub enum GameClientMessage {
    Connected(String),
    Disconnected {
        name: String,
        had_cards: bool,
    },
    Play {
        card: Card,
        next_turn: usize,
        attempts_left: usize,
        missed_slap: u32,
    },
    Slap {
        name: String,
        slap: SlapType,
        time: u64,
        second_best_time: Option<u64>,
    },
    NewDeck(Deck),
    NewCard(Card),
    NewGame,
    PlayerList {
        players: Vec<String>,
        next_turn: usize,
    },
    Burn {
        name: String,
        card: Option<Card>,
    },
    OutOfGame(String),
    WonPile(String),
    CollectedDeck {
        name: String,
        missed_slap: bool,
    },
    UpdateCardCount,
    GameWon {
        name: String,
        cards_left: usize,
    },
    NameInUse,
}

#[derive(Clone, Debug)]
pub enum SlapType {
    Double,
    Sandwich,
}

// Type of message to be passed between front and back end
#[derive(Serialize, Deserialize)]
pub struct JsonMsg {
    pub action: usize,
    pub content: Option<Value>,
}

// Type of action for JsonMsg
pub enum JsonAction {
    Connect,
    NewGame,
    Slap,
    Draw,
    Text,
}

impl From<usize> for JsonAction {
    fn from(num: usize) -> JsonAction {
        match num {
            0 => JsonAction::Connect,
            1 => JsonAction::NewGame,
            2 => JsonAction::Slap,
            3 => JsonAction::Draw,
            4 => JsonAction::Text,
            _ => JsonAction::Slap,
        }
    }
}

impl GameServer {
    pub fn new() -> GameServer {
        GameServer {
            sessions: HashMap::new(),
            pile: Deck::new(),
            current_turn_idx: 0,
            turn_order: Vec::new(),
            facecard_attempts_left: None,
            faceoff_winner: None,
            slap_helper_addr: None,
            slapped: Instant::now(),
        }
    }
}

pub struct CardPlayed;

pub struct GetName;

pub struct HandEmpty;

pub struct CardBack(pub Card);

pub struct BurnCard;

pub struct IsBanned;

impl Message for CardPlayed {
    type Result = ();
}

impl Message for IsBanned {
    type Result = bool;
}

impl Message for BurnCard {
    type Result = Option<Card>;
}

impl Message for CardBack {
    type Result = ();
}

impl Message for HandEmpty {
    type Result = bool;
}

impl Message for GetName {
    type Result = Option<String>;
}

impl Message for GameServerMessage {
    type Result = Result<(), String>;
}

impl Message for GameClientMessage {
    type Result = Option<String>;
}
