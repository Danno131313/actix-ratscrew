use actix::Addr;
use std::{collections::HashMap, sync::Mutex, time::Instant};
use uuid::Uuid;

use crate::models::game::GameServer;

pub struct SlapHelper {
    pub slappers: Mutex<HashMap<Uuid, u64>>,
    pub server_addr: Addr<GameServer>,
    pub card_slapped: Instant,
}

impl SlapHelper {
    pub fn new(addr: Addr<GameServer>) -> SlapHelper {
        SlapHelper {
            slappers: Mutex::new(HashMap::new()),
            server_addr: addr,
            card_slapped: Instant::now(),
        }
    }
}

pub struct CollectAndSend;

pub struct SlapMsg(pub HashMap<Uuid, u64>);

pub struct SlapHelperAddr(pub Addr<SlapHelper>);
