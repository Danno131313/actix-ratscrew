use actix::*;
use actix_web::ws;
use log::info;
use serde_json::{from_str, json};
use simple_cards::Card;
use std::time::{Duration, Instant};

use crate::models::{
    game::{
        BurnCard, CardBack, GameClientMessage, GameServerMessage, GetName, HandEmpty, IsBanned,
        JsonAction, JsonMsg, SlapType, WsGameSession,
    },
    ws::{Connect, Disconnect, GetKicked, WsGameSessionState},
};

impl Actor for WsGameSession {
    type Context = ws::WebsocketContext<Self, WsGameSessionState>;

    // Runs when websocket connection is initiated
    fn started(&mut self, ctx: &mut Self::Context) {
        self.hb(ctx);

        let addr = ctx.address();
        ctx.state()
            .addr
            .send(Connect { addr })
            .into_actor(self)
            .then(|res, act, ctx| {
                match res {
                    Ok(res) => act.id = res.expect("Couldn't get id from server"),
                    _ => ctx.stop(),
                }
                fut::ok(())
            })
            .wait(ctx);
    }

    // Runs when websocket connection is dropped
    fn stopping(&mut self, ctx: &mut Self::Context) -> Running {
        let deck = if self.hand.is_empty() {
            None
        } else {
            Some(self.hand.clone())
        };
        ctx.state().addr.do_send(Disconnect {
            name: self.name.to_owned(),
            id: self.id,
            deck,
        });
        Running::Stop
    }
}
impl WsGameSession {
    // Function to ping the client every 10 seconds to keep connection alive
    fn hb(&self, ctx: &mut ws::WebsocketContext<Self, WsGameSessionState>) {
        ctx.run_interval(Duration::from_secs(10), |act, ctx| {
            if Instant::now().duration_since(act.hb) > Duration::from_secs(15) {
                info!("Heartbeat failed for {}, disconnecting!", act.name);

                let deck = if act.hand.is_empty() {
                    None
                } else {
                    Some(act.hand.clone())
                };
                ctx.state().addr.do_send(Disconnect {
                    id: act.id,
                    name: act.name.clone(),
                    deck,
                });

                ctx.stop();
                return;
            }

            ctx.ping("");
        });
    }
}

impl StreamHandler<ws::Message, ws::ProtocolError> for WsGameSession {
    // Handler for when client recieves websocket message from the front end
    fn handle(&mut self, msg: ws::Message, ctx: &mut Self::Context) {
        match msg {
            ws::Message::Text(text) => {
                let json: JsonMsg = from_str(&text).expect("Couldn't parse JSON");

                match json.action.into() {
                    JsonAction::Connect => {
                        let name = json.content.expect("Couldn't parse JSON connect name");
                        self.name = name
                            .as_str()
                            .expect("Couldn't convert String to str")
                            .to_string();
                        ctx.state().addr.do_send(GameServerMessage::Connected {
                            name: self.name.clone(),
                            id: self.id,
                        });
                    }
                    JsonAction::NewGame => {
                        ctx.state().addr.do_send(GameServerMessage::NewGame);
                    }
                    JsonAction::Slap => {
                        let content = json.content.expect("No content in slap json!");
                        let time = content.as_u64().expect("Couldn't cast to u64");
                        ctx.state().addr.do_send(GameServerMessage::Slap {
                            id: self.id,
                            name: self.name.clone(),
                            time,
                        });
                    }
                    JsonAction::Draw => {
                        if !self.hand.is_empty() {
                            let card = self.hand.draw().expect("Couldn't draw card from hand");
                            let last_card = self.hand.is_empty();
                            ctx.state().addr.do_send(GameServerMessage::Play {
                                id: self.id,
                                name: self.name.clone(),
                                card,
                                last_card,
                            });
                        }
                    }
                    _ => (),
                }
            }
            ws::Message::Pong(_) => self.hb = Instant::now(),
            _ => (),
        }
    }
}

impl Handler<GameClientMessage> for WsGameSession {
    type Result = Option<String>;

    // Handler for when the client gets a message from the server, then sends it to the front end.
    // Recieves GameClientMessage, returns a stringified JsonMsg
    fn handle(&mut self, msg: GameClientMessage, ctx: &mut Self::Context) -> Self::Result {
        let json = match msg {
            GameClientMessage::Connected(name) => JsonMsg {
                action: 0,
                content: Some(json!(name)),
            },

            GameClientMessage::NewGame => {
                self.hand.clear();
                JsonMsg {
                    action: 1,
                    content: None,
                }
            }

            GameClientMessage::Slap {
                name,
                slap,
                time,
                second_best_time,
            } => {
                let slap_type = match slap {
                    SlapType::Double => 1,
                    SlapType::Sandwich => 2,
                };
                JsonMsg {
                    action: 2,
                    content: Some(
                        json!({"name": name, "type": slap_type, "time": time, "second_best_time": second_best_time}),
                    ),
                }
            }

            GameClientMessage::Play {
                card,
                next_turn,
                attempts_left,
                missed_slap,
            } => JsonMsg {
                action: 3,
                content: Some(
                    json!({ "card": {"value": card.value as usize, "suit": card.suit as usize}, "next_turn": next_turn, "attempts_left": attempts_left, "missed_slap": missed_slap}),
                ),
            },

            GameClientMessage::PlayerList { players, next_turn } => JsonMsg {
                action: 5,
                content: Some(json!({"players": players, "next_turn": next_turn})),
            },

            GameClientMessage::NewCard(card) => {
                self.hand.add(card);
                JsonMsg {
                    action: 6,
                    content: None,
                }
            }

            GameClientMessage::Disconnected { name, had_cards } => JsonMsg {
                action: 7,
                content: Some(json!({"name": name, "had_cards": had_cards})),
            },

            GameClientMessage::OutOfGame(name) => JsonMsg {
                action: 8,
                content: Some(json!(name)),
            },

            GameClientMessage::WonPile(name) => JsonMsg {
                action: 9,
                content: Some(json!(name)),
            },

            GameClientMessage::CollectedDeck { name, missed_slap } => JsonMsg {
                action: 10,
                content: Some(json!({"name": name, "missed_slap": missed_slap})),
            },

            GameClientMessage::NewDeck(mut deck) => {
                let len = deck.len();
                self.hand.add_deck(&mut deck);
                if self.hand.len() > 42 {
                    ctx.state().addr.do_send(GameServerMessage::GameWon {
                        name: self.name.clone(),
                        cards_left: self.hand.len(),
                    });
                }
                JsonMsg {
                    action: 11,
                    content: Some(json!(len)),
                }
            }

            GameClientMessage::UpdateCardCount => JsonMsg {
                action: 12,
                content: Some(json!(self.hand.len())),
            },

            GameClientMessage::GameWon { name, cards_left } => {
                self.hand.clear();
                JsonMsg {
                    action: 13,
                    content: Some(json!({"name": name, "cards_left": cards_left})),
                }
            }

            GameClientMessage::Burn { name, card } => JsonMsg {
                action: 14,
                content: Some(json!({"name": name, "card": card})),
            },

            GameClientMessage::NameInUse => JsonMsg {
                action: 15,
                content: None,
            },
        };

        ctx.text(serde_json::to_string(&json).expect("Couldn't serialize JSON response"));
        if json.action == 15 {
            ctx.stop();
        }

        None
    }
}

impl Handler<GetName> for WsGameSession {
    type Result = Option<String>;

    fn handle(&mut self, _: GetName, _: &mut Self::Context) -> Self::Result {
        Some(self.name.clone())
    }
}

impl Handler<HandEmpty> for WsGameSession {
    type Result = bool;

    fn handle(&mut self, _: HandEmpty, _: &mut Self::Context) -> Self::Result {
        self.hand.is_empty()
    }
}

impl Handler<CardBack> for WsGameSession {
    type Result = ();

    fn handle(&mut self, msg: CardBack, _: &mut Self::Context) {
        self.hand.add_bottom(msg.0);
    }
}

impl Handler<BurnCard> for WsGameSession {
    type Result = Option<Card>;

    fn handle(&mut self, _: BurnCard, _: &mut Self::Context) -> Self::Result {
        if self.hand.is_empty() {
            self.missed_slaps += 1;
            if self.missed_slaps >= 3 {
                self.banned = true;
            }
            None
        } else {
            Some(self.hand.draw().expect("Couldn't burn own card"))
        }
    }
}

impl Handler<IsBanned> for WsGameSession {
    type Result = bool;

    fn handle(&mut self, _: IsBanned, _: &mut Self::Context) -> Self::Result {
        self.banned
    }
}

impl Handler<GetKicked> for WsGameSession {
    type Result = ();

    fn handle(&mut self, _: GetKicked, ctx: &mut Self::Context) {
        ctx.stop();
    }
}
