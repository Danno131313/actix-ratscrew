use actix::*;
use actix_web::Error;
use futures::Future;
use log::info;
use simple_cards::{Card, Deck};
use std::time::{Duration, Instant};
use uuid::Uuid;

use crate::{
    game::misc::get_attempts,
    models::{
        game::{
            BurnCard, CardBack, CardPlayed, GameClientMessage, GameServer, GameServerMessage,
            GetName, HandEmpty, IsBanned, SlapType, WsGameSession,
        },
        slap_helper::{SlapHelperAddr, SlapMsg},
        ws::{Connect, Disconnect, GetKicked, KickPlayer},
    },
};

impl Actor for GameServer {
    type Context = Context<Self>;
}

impl GameServer {
    // Send GameClientMessage to all connected clients
    pub fn broadcast_all(&self, msg: &GameClientMessage) {
        for addr in self.sessions.values() {
            addr.do_send(msg.clone());
        }
    }

    // Send to all clients excluding one UUID
    pub fn broadcast_excluding(&self, msg: &GameClientMessage, skip_id: Uuid) {
        for (id, addr) in &self.sessions {
            if *id == skip_id {
                continue;
            } else {
                addr.do_send(msg.clone());
            }
        }
    }

    // Get the name of a client from their UUID
    pub fn get_name(&self, id: &Uuid) -> Option<String> {
        let addr = &self
            .sessions
            .get(id)
            .expect("Couldn't find that UUID in sessions for get_name");
        addr.send(GetName).wait().unwrap_or_else(|_| {
            addr.do_send(GetKicked);
            None
        })
    }

    // Send the list of all clients to every client
    pub fn send_player_list(&self) {
        let mut players = Vec::new();
        for id in &self.turn_order {
            players.push(
                self.get_name(id)
                    .expect("Error sending player list to an id"),
            );
        }
        self.broadcast_all(&GameClientMessage::PlayerList {
            players,
            next_turn: self.current_turn_idx,
        });
    }

    // Helper for next_turn()
    pub fn next(&mut self) {
        self.current_turn_idx += 1;
        if self.turn_order.is_empty() || self.current_turn_idx >= self.turn_order.len() {
            self.current_turn_idx = 0;
        }
    }

    // Helper for prev_turn()
    pub fn prev(&mut self) {
        if self.turn_order.is_empty() {
            self.current_turn_idx = 0;
        } else if self.current_turn_idx == 0 {
            self.current_turn_idx = self.turn_order.len() - 1;
        } else {
            self.current_turn_idx -= 1;
        }
    }

    // Advance the turn to the next in the turn order
    pub fn next_turn(&mut self) {
        let last_idx = self.current_turn_idx;
        self.next();
        while last_idx != self.current_turn_idx {
            {
                let id = self.turn_order[self.current_turn_idx];
                let addr = &self.sessions[&id];
                let res = addr.send(HandEmpty).wait();
                match res {
                    Ok(hand_empty) => {
                        if !hand_empty {
                            break;
                        }
                    }
                    Err(_) => addr.do_send(GetKicked),
                }
            }
            self.next();
        }
    }

    // Go back to the last player to play
    // (Used for when someone fails a faceoff)
    pub fn prev_turn(&mut self) {
        let last_idx = self.current_turn_idx;
        self.prev();

        while last_idx != self.current_turn_idx {
            {
                let id = self.turn_order[self.current_turn_idx];
                let addr = &self.sessions[&id];
                let res = addr.send(HandEmpty).wait();
                match res {
                    Ok(hand_empty) => {
                        if !hand_empty {
                            break;
                        }
                    }
                    Err(_) => addr.do_send(GetKicked),
                }
            }
            self.prev();
        }
    }

    // Checks whether or not a slap is correct with the current state of the pile
    pub fn verify_slap(&self) -> Option<SlapType> {
        let last_idx = self.pile.len() - 1;

        if self.pile.len() <= 1 {
            None
        } else if self.pile.len() == 2 {
            let card1 = self.pile.show(0).unwrap();
            let card2 = self.pile.show(1).unwrap();

            if card1.value == card2.value {
                Some(SlapType::Double)
            } else {
                None
            }
        } else {
            let card1 = self.pile.show(last_idx).unwrap();
            let card2 = self.pile.show(last_idx - 1).unwrap();
            let card3 = self.pile.show(last_idx - 2).unwrap();

            if card1.value == card2.value {
                Some(SlapType::Double)
            } else if card1.value == card3.value {
                Some(SlapType::Sandwich)
            } else {
                None
            }
        }
    }

    // Main logic for when a card is played
    pub fn process_play(&mut self, card: Card) {
        let mut name = String::new();
        let mut won_pile = false;

        self.pile.add(card);

        if card.is_face() {
            self.facecard_attempts_left = Some(get_attempts(card));
            self.next_turn();
        } else if self.facecard_attempts_left.is_some() {
            let num = self
                .facecard_attempts_left
                .expect("No facecard attempts left when should be");
            self.facecard_attempts_left = Some(num - 1);
            if self
                .facecard_attempts_left
                .expect("No facecard attempts left")
                == 0
            {
                self.prev_turn();
                let id = self.turn_order[self.current_turn_idx];
                name = self.get_name(&id).expect("get_name returned none");
                won_pile = true;
                self.facecard_attempts_left = None;
                self.faceoff_winner = Some(id);
            }
        } else {
            self.next_turn();
        }

        let missed_slap = self.missed_slap();
        let slap_code = if missed_slap.is_some() {
            let slap_type = missed_slap.unwrap();
            match slap_type {
                SlapType::Double => 1,
                SlapType::Sandwich => 2,
            }
        } else {
            0
        };
        self.broadcast_all(&GameClientMessage::Play {
            card,
            next_turn: self.current_turn_idx,
            attempts_left: self.facecard_attempts_left.unwrap_or(0),
            missed_slap: slap_code,
        });

        if won_pile {
            self.broadcast_all(&GameClientMessage::WonPile(name));
        }
    }

    // Send pile to a client
    pub fn send_deck(&mut self, addr: &Addr<WsGameSession>, name: String, id: Uuid) {
        addr.do_send(GameClientMessage::NewDeck(self.pile.clone()));
        let missed_slap = self.missed_slap().is_some();
        self.pile.clear();
        self.faceoff_winner = None;
        self.broadcast_excluding(&GameClientMessage::CollectedDeck { name, missed_slap }, id);
    }

    pub fn process_slap(
        &mut self,
        id: Uuid,
        name: String,
        time: u64,
        second_best_time: Option<u64>,
    ) {
        let addr = self
            .sessions
            .get(&id)
            .expect("That ID isn't in sessions")
            .clone();

        if self.pile.is_empty() || addr.send(IsBanned).wait().expect("Bad IsBanned response") {
            return;
        }

        if self.faceoff_winner.is_some()
            && self.faceoff_winner.expect("Expected faceoff_winner") == id
        {
            self.send_deck(&addr, name, id);
        } else {
            let slap = self.verify_slap();

            if slap.is_some() {
                let mut idx = 0;
                for (i, uuid) in self.turn_order.iter().enumerate() {
                    if id == *uuid {
                        idx = i;
                    }
                }

                self.current_turn_idx = idx;
                self.facecard_attempts_left = None;
                self.faceoff_winner = None;

                self.send_player_list();

                self.broadcast_all(&GameClientMessage::Slap {
                    name: name.clone(),
                    slap: slap.unwrap(),
                    time,
                    second_best_time,
                });

                self.send_deck(&addr, name, id);
            } else {
                let card = addr
                    .send(BurnCard)
                    .wait()
                    .expect("Expected burncard to succeed");

                if card.is_some() {
                    self.broadcast_all(&GameClientMessage::Burn { name, card });

                    self.pile.add_bottom(card.expect("Couldn't unwrap card 2"));
                } else {
                    self.broadcast_all(&GameClientMessage::Burn { name, card: None });
                }
            }
        }
        addr.do_send(GameClientMessage::UpdateCardCount);
    }

    pub fn missed_slap(&self) -> Option<SlapType> {
        if self.pile.len() < 3 {
            None
        } else {
            let last_idx = self.pile.len() - 1;
            if self.pile.show(last_idx - 1).unwrap().value
                == self.pile.show(last_idx - 2).unwrap().value
            {
                Some(SlapType::Double)
            } else if self.pile.len() >= 4 {
                if self.pile.show(last_idx - 1).unwrap().value
                    == self.pile.show(last_idx - 3).unwrap().value
                {
                    Some(SlapType::Sandwich)
                } else {
                    None
                }
            } else {
                None
            }
        }
    }
}

// New connection handler for server
impl Handler<Connect> for GameServer {
    type Result = Result<Uuid, Error>;

    fn handle(&mut self, msg: Connect, _: &mut Context<Self>) -> Self::Result {
        info!("Someone joined");

        let id = Uuid::new_v4();

        self.turn_order.push(id);

        self.sessions.insert(id, msg.addr);

        Ok(id)
    }
}

// Disconnect handler for the server
impl Handler<Disconnect> for GameServer {
    type Result = ();

    fn handle(&mut self, msg: Disconnect, _: &mut Context<Self>) {
        info!("User {} has disconnected", msg.name);

        self.sessions.remove(&msg.id);

        let mut idx: usize = 0;
        for (i, id) in self.turn_order.iter().enumerate() {
            if *id == msg.id {
                idx = i;
            }
        }

        self.turn_order.remove(idx);
        self.next_turn();

        let had_cards = msg.deck.is_some();
        if had_cards && !self.sessions.is_empty() {
            let mut deck = msg.deck.expect("Couldn't get deck from msg");
            let sessions = self.sessions.clone();
            let mut addrs_cycle = sessions.values().cycle();

            while !deck.is_empty() {
                let addr = addrs_cycle.next().expect("Error cycling through addrs");
                if !addr.send(IsBanned).wait().expect("IsBanned response error") {
                    addr.do_send(GameClientMessage::NewCard(
                        deck.draw()
                            .expect("Error drawing card from disconnected client's deck"),
                    ));
                }
            }

            for addr in sessions.values() {
                addr.do_send(GameClientMessage::UpdateCardCount);
            }
        }

        if idx == self.current_turn_idx && idx >= self.turn_order.len() {
            self.current_turn_idx = 0;
        }

        self.broadcast_excluding(
            &GameClientMessage::Disconnected {
                name: msg.name,
                had_cards,
            },
            msg.id,
        );
        self.send_player_list();
    }
}

// Handler for every other type of message to the server
impl Handler<GameServerMessage> for GameServer {
    type Result = Result<(), String>;

    fn handle(&mut self, msg: GameServerMessage, _: &mut Self::Context) -> Self::Result {
        match msg {
            GameServerMessage::Connected { name, id } => {
                let mut names: Vec<String> = Vec::new();
                for user_id in self.sessions.keys() {
                    if id == *user_id {
                        continue;
                    }
                    let name = self.get_name(user_id).unwrap();
                    names.push(name);
                }

                if names.contains(&name) {
                    let addr = &self.sessions[&id];
                    addr.do_send(GameClientMessage::NameInUse);
                } else {
                    info!("User {} has connected with id {}", name, id);
                    self.broadcast_all(&GameClientMessage::Connected(name));
                    self.send_player_list();
                }

                Ok(())
            }

            GameServerMessage::NewGame => {
                if self.sessions.len() <= 1 {
                    return Ok(());
                }

                self.broadcast_all(&GameClientMessage::NewGame);
                self.pile = Deck::default();
                self.facecard_attempts_left = None;
                self.faceoff_winner = None;
                let sessions = self.sessions.clone();
                let mut addrs_cycle = sessions.values().cycle();

                while !self.pile.is_empty() {
                    let addr = addrs_cycle.next().expect("Couldn't get next addr");
                    if !addr
                        .send(IsBanned)
                        .wait()
                        .expect("Couldn't get response for IsBanned")
                    {
                        addr.do_send(GameClientMessage::NewCard(
                            self.pile.draw().expect("Couldn't get card from pile"),
                        ));
                    }
                }

                for addr in sessions.values() {
                    addr.do_send(GameClientMessage::UpdateCardCount);
                }

                Ok(())
            }

            GameServerMessage::Slap { name, id, time } => {
                self.slapped = Instant::now();
                self.slap_helper_addr
                    .clone()
                    .expect("Couldn't expect slap_helper_addr")
                    .do_send(GameServerMessage::Slap {
                        name: name.clone(),
                        id,
                        time,
                    });

                Ok(())
            }

            // GameServerMessage::SlapResult {}
            GameServerMessage::Play {
                id,
                name,
                card,
                last_card,
            } => {
                if self.slapped.elapsed() > Duration::from_millis(1500) {
                    let addr = self.sessions[&id].clone();
                    if self.faceoff_winner.is_some()
                        && self.faceoff_winner.expect("Faceoff winner was none") == id
                    {
                        addr.do_send(CardBack(card));
                        self.send_deck(&addr, name, id);
                    } else {
                        if self.turn_order[self.current_turn_idx] == id {
                            self.process_play(card);
                            self.slap_helper_addr
                                .clone()
                                .expect("No server addr!")
                                .do_send(CardPlayed);
                        } else {
                            addr.do_send(CardBack(card));
                            addr.do_send(GameClientMessage::UpdateCardCount);
                            return Ok(());
                        }

                        if last_card {
                            self.broadcast_all(&GameClientMessage::OutOfGame(name));
                        }
                    }
                    addr.do_send(GameClientMessage::UpdateCardCount);
                }

                Ok(())
            }

            GameServerMessage::GameWon { name, cards_left } => {
                self.broadcast_all(&GameClientMessage::GameWon { name, cards_left });
                Ok(())
            }
        }
    }
}

impl Handler<SlapMsg> for GameServer {
    type Result = ();

    fn handle(&mut self, msg: SlapMsg, _: &mut Self::Context) {
        let slaps = msg.0;
        let mut min = u64::max_value();
        let mut second_min: Option<u64> = None;
        let mut min_id = Uuid::nil();
        for (id, time) in slaps {
            if min != u64::max_value()
                && time < second_min.unwrap_or(u64::max_value())
                && id != min_id
            {
                if time < min {
                    second_min = Some(min);
                } else {
                    second_min = Some(time);
                }
            }
            if time < min {
                min = time;
                min_id = id;
            }
        }
        let name = self.get_name(&min_id).expect("No name by that id");
        self.process_slap(min_id, name, min, second_min);
    }
}

impl Handler<SlapHelperAddr> for GameServer {
    type Result = ();

    fn handle(&mut self, msg: SlapHelperAddr, _: &mut Self::Context) {
        self.slap_helper_addr = Some(msg.0);
    }
}

impl Handler<KickPlayer> for GameServer {
    type Result = ();

    fn handle(&mut self, msg: KickPlayer, _: &mut Self::Context) {
        let addr = self
            .sessions
            .get(&msg.0)
            .expect("Couldn't find uuid to kick");
        addr.do_send(GetKicked);
    }
}
