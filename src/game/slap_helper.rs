use actix::*;
use std::time::{Duration, Instant};

use crate::models::{
    game::{CardPlayed, GameServerMessage},
    slap_helper::{CollectAndSend, SlapHelper, SlapHelperAddr, SlapMsg},
};

impl Actor for SlapHelper {
    type Context = Context<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        self.server_addr.do_send(SlapHelperAddr(ctx.address()));
    }
}

impl Message for SlapMsg {
    type Result = ();
}

impl Message for SlapHelperAddr {
    type Result = ();
}

impl Message for CollectAndSend {
    type Result = ();
}

impl Handler<GameServerMessage> for SlapHelper {
    type Result = Result<(), String>;

    fn handle(&mut self, msg: GameServerMessage, ctx: &mut Self::Context) -> Self::Result {
        if let GameServerMessage::Slap { id, time, .. } = msg {
            self.card_slapped = Instant::now();
            let mut slappers = self.slappers.lock().unwrap();
            slappers.entry(id).or_insert(time);
            ctx.run_later(Duration::from_millis(1500), |_, ctx| {
                let addr = ctx.address();
                addr.do_send(CollectAndSend);
            });
        }

        Ok(())
    }
}

impl Handler<CardPlayed> for SlapHelper {
    type Result = ();

    fn handle(&mut self, _: CardPlayed, _: &mut Self::Context) {
        let mut slappers = self.slappers.lock().unwrap();
        slappers.clear();
    }
}

impl Handler<CollectAndSend> for SlapHelper {
    type Result = ();

    fn handle(&mut self, _: CollectAndSend, _: &mut Self::Context) {
        let mut slappers = self.slappers.lock().unwrap();
        if self.card_slapped.elapsed() >= Duration::from_millis(1500) && !slappers.is_empty() {
            self.server_addr.do_send(SlapMsg(slappers.clone()));
            slappers.clear();
        }
    }
}
