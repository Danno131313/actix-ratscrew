use simple_cards::{cards::Value, Card};

pub fn get_attempts(card: Card) -> usize {
    match card.value {
        Value::Jack => 1,
        Value::Queen => 2,
        Value::King => 3,
        Value::Ace => 4,
        _ => 0,
    }
}
