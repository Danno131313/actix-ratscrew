use actix_web::{error, ws, Error, HttpRequest, HttpResponse, Path, Result, State};
use simple_cards::Deck;
use std::time::Instant;
use uuid::Uuid;

use crate::models::{
    game::WsGameSession,
    ws::{KickPlayer, WsGameSessionState},
};

pub fn game_route(req: &HttpRequest<WsGameSessionState>) -> Result<HttpResponse, Error> {
    ws::start(
        req,
        WsGameSession {
            id: Uuid::new_v4(),
            name: "Me".to_string(),
            hand: Deck::new_empty(),
            hb: Instant::now(),
            missed_slaps: 0,
            banned: false,
        },
    )
}

pub fn kick_player(
    (state, path): (State<WsGameSessionState>, Path<String>),
) -> Result<HttpResponse, Error> {
    let id = Uuid::parse_str(&path.into_inner()).map_err(|_| error::ErrorBadRequest("Bad uuid"))?;
    state.addr.do_send(KickPlayer(id));
    Ok(HttpResponse::Ok().finish())
}
